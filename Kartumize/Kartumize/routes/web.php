<?php

use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', 'DesignController@home');
Route::get('/home','DesignController@home');

Route::get('/designs', 'DesignController@designs');

Route::get('/aboutus', 'DesignController@aboutus');

Route::get('/submit', 'DesignController@submitCustomPage');
Route::post('/submit', 'DesignController@submitCustomDesign');

Route::get('/login', 'UserController@loginPage');
Route::post('/login', 'UserController@login');

Route::get('/register', 'UserController@registerPage');
Route::post('/register', 'UserController@register');

Route::get('/logout', 'UserController@logout');

Route::get('/details/{id}', 'DesignController@details');

Route::get('/addToCart/{id}', 'CartController@addToCartPage');
Route::post('/addToCart/{id}', 'CartController@addToCart');

Route::get('/viewCart', 'CartController@viewCartPage');
Route::post('/deleteItem', 'CartController@deleteCartItem');
Route::post('/editItem', 'CartController@editCartItem');
Route::post('/checkout', 'CartController@checkout');

Route::get('/adminHome', 'UserController@adminHome');

Route::get('/addDesign', 'DesignController@addDesignPage');
Route::post('/addDesign', 'DesignController@addDesign');
Route::get('/viewDesign', 'DesignController@viewDesign');
Route::post('/deleteDesign', 'DesignController@deleteDesign');

Route::get('/addCategory', 'CategoryController@addCategoryPage');
Route::post('/addCategory', 'CategoryController@addCategory');
Route::get('/viewCategory', 'CategoryController@viewCategory');
Route::get('/viewCategoryDetail/{id}', 'CategoryController@viewCategoryDetail');