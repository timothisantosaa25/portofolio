<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Item extends Model
{
    public function design(){
        return $this->hasMany(Design::class, 'id', 'design_id');
    }

    public function cart(){
        return $this->belongsToMany(Cart::class);
    }

    protected $fillable = [
        'cart_id', 'design_id', 'quantity',
    ];

    protected $table = 'item';
}
