<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Category extends Model
{
    public function design(){
        return $this->belongsToMany(Design::class);
    }

    protected $fillable = [
        'name',
    ];

    protected $table = 'category';
}
