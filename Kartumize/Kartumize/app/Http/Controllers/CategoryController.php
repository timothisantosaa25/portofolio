<?php

namespace App\Http\Controllers;

use App\Category;
use App\Design;
use Illuminate\Http\Request;

class CategoryController extends Controller
{
    public function addCategoryPage(){
        $category = Category::all();

        return view('addCategory', ['category' => $category]);
    }

    public function addCategory(Request $request){
        $request->validate([
            'name' => 'required|unique:category,name',
        ]);

        Category::create([
            'name' => $request->name, 
        ]);

        return redirect('/adminHome');
    }

    public function viewCategory(){
        $category = Category::all();

        return view('viewCategory', ['category' => $category]);
    }

    public function viewCategoryDetail($id){
        $category = Category::all();
        $designs = Design::where('category_id', 'like', $id)->paginate();

        return view('viewCategoryDetail', ['category' => $category, 'designs' => $designs]);
    }
}
