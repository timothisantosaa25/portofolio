<?php

namespace App\Http\Controllers;

use App\User;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Foundation\Validation\ValidatesRequests;
use Illuminate\Support\Facades\Validator;
use Illuminate\Validation\Rule;

class UserController extends Controller
{
    public function adminHome(){
        return view('adminHome');
    }

    public function registerPage(){
        return view('register');
    }

    public function register(Request $request){
        $pass = $request->get('password');
        $conPass = $request->get('confirmPassword');

        $request->validate([
            'username' => 'required',
            'email' => 'required|email|unique:users,email',
            'password' => 'required|min:5',
            'confirmPassword' => 'required',
        ]);

        if($conPass == $pass){
            User::create([
                'username' => $request->username,
                'email' => $request->email,
                'password' => bcrypt($request->password),
                'role' => 'member',
            ]);
    
            return redirect('/');
        }
        else{
            $request->validate([
                'confirmPassword' => 'same:password',
            ]);
            return back();
        }
    }

    public function loginPage(){
        return view('login');
    }

    public function login(Request $request){
        $request->validate([
            'email' => 'required|email',
            'password' => 'required|min:5',
        ]);

        $info = $request->only('email', 'password');

        $role = 'guest';

        if (Auth::viaRemember()) {
            $auth = Auth::check();
            if($auth){
                $role = Auth::user()->role;
                $user = Auth::user()->username;
            }

            if($role == 'member'){
                return redirect('/');
            }
            else{
                return redirect('/adminHome');
            }
        }
        if ($request->get('remember_token')) {
            if(Auth::attempt($info, true)){
                $auth = Auth::check();
                if($auth){
                    $role = Auth::user()->role;
                    $user = Auth::user()->username;
                }

                if($role == 'member'){
                    return redirect('/');
                }
                else{
                    return redirect('/adminHome');
                }
            }
            else{
                $request->validate([
                    'email' => 'exists:users,email|exists:users,password',
                    'password' => 'exists:users,password|exists:users,email',
                ]);
                return back();
            }
        }
        else{
            if(Auth::attempt($info)){
                $auth = Auth::check();
                if($auth){
                    $role = Auth::user()->role;
                    $user = Auth::user()->username;
                }

                if($role == 'member'){
                    return redirect('/');
                }
                else{
                    return redirect('/adminHome');
                }
            }
            else{
                $request->validate([
                    'email' => 'exists:users,email|exists:users,password',
                    'password' => 'exists:users,password|exists:users,email',
                ]);
                return back();
            }
        }
    }

    public function logout(){
        Auth::logout();

        return redirect('/');
    }
}
