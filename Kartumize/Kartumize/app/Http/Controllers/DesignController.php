<?php

namespace App\Http\Controllers;

use App\Design;
use App\Cart;
use App\Category;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;

class DesignController extends Controller
{
    public function home(Request $request){
        $search = $request->input('search');

        $designs = Design::paginate(4);

        $designs = Design::where('name', 'like', "%$search%")->paginate(4);

        $auth = Auth::check();
        $role = 'guest';
        $user = '';
        $quantity = 0;

        if($auth){
            $role = Auth::user()->role;
            $user = Auth::user()->username;
            $cart = Cart::where('user_id','like', Auth::user()->id)->paginate();
            if($cart->isNotEmpty()){
                $quantity = Auth::user()->cart->quantity;
            }
        }

        return view('home', ['designs' => $designs, 'auth'=> $auth, 'role' => $role, 'user' => $user, 'quantity' => $quantity]);
    }

    public function details(Request $request, $id){
        $designs = Design::where('id', 'like', $id)->paginate();

        $search = $request->input('search');

        if($search != ''){
            $designs = Design::where('name', 'like', "%$search%")->paginate(4);
        }

        $auth = Auth::check();
        $role = 'guest';
        $user = '';
        $quantity = 0;

        if($auth){
            $role = Auth::user()->role;
            $user = Auth::user()->username;
            $cart = Cart::where('user_id','like', Auth::user()->id)->paginate();
            if($cart->isNotEmpty()){
                $quantity = Auth::user()->cart->quantity;
            }
        }

        return view('details', ['designs' => $designs, 'auth'=> $auth, 'role' => $role, 'user' => $user, 'quantity' => $quantity]);
    }

    public function addDesignPage(){
        $category = Category::all();

        return view('addDesign', ['category' => $category]);
    }

    public function addDesign(Request $request){
        $request->validate([
            'name' => 'required|unique:designs,name',
            'category' => 'required|not_in:0',
            'description' => 'required',
            'price' => 'required|integer|min:100',
            'image' => 'required|image|max:10000',
        ]);

        Design::create([
            'category_id' => $request->category, 
            'name' => $request->name, 
            'description' => $request->description, 
            'price' => $request->price, 
            'image' => $request->image->store('assets', 'public'),
        ]);

        return redirect('/adminHome');
    }

    public function submitCustomPage(){
        $category = Category::all();

        $auth = Auth::check();
        $role = 'guest';
        $user = '';
        $quantity = 0;

        if($auth){
            $role = Auth::user()->role;
            $user = Auth::user()->username;
            $cart = Cart::where('user_id','like', Auth::user()->id)->paginate();
            if($cart->isNotEmpty()){
                $quantity = Auth::user()->cart->quantity;
            }
        }

        return view('submitCustomDesign', ['category' => $category, 'auth' => $auth, 'role' => $role, 'user' => $user, 'quantity' => $quantity]);
    }

    public function submitCustomDesign(Request $request){
        $request->validate([
            'name' => 'required|unique:designs,name',
            'category' => 'required|not_in:0',
            'description' => 'required',
            'price' => 'required|integer|min:100',
            'image' => 'required|image|max:10000',
        ]);

        Design::create([
            'category_id' => $request->category, 
            'name' => $request->name, 
            'description' => $request->description, 
            'price' => $request->price, 
            'image' => $request->image->store('assets', 'public'),
        ]);

        return redirect('/submit');
    }

    public function viewDesign(){
        $designs = Design::all();
        $category = Category::all();

        return view('viewDesign', ['designs' => $designs, 'category' => $category]);
    }

    public function deleteDesign(Request $request){
        Design::where('id', $request->id)->delete();
        
        return back();
    }

    public function designs () {
        $anime = Design::where('category_id', 1)->get();
        $brand = Design::where('category_id', 2)->get();
        $movie = Design::where('category_id', 3)->get();
        $game  = Design::where('category_id', 4)->get();

        $auth = Auth::check();
        $role = 'guest';
        $user = '';
        $quantity = 0;

        if($auth){
            $role = Auth::user()->role;
            $user = Auth::user()->username;
            $cart = Cart::where('user_id','like', Auth::user()->id)->paginate();
            if($cart->isNotEmpty()){
                $quantity = Auth::user()->cart->quantity;
            }
        }

        return view('designs', ['anime'=>$anime,'brand'=>$brand,'movie'=>$movie,'game'=>$game,'auth'=> $auth, 'role' => $role, 'user' => $user, 'quantity' => $quantity]);
    }

    public function aboutus () {

        $auth = Auth::check();
        $role = 'guest';
        $user = '';
        $quantity = 0;

        if($auth){
            $role = Auth::user()->role;
            $user = Auth::user()->username;
            $cart = Cart::where('user_id','like', Auth::user()->id)->paginate();
            if($cart->isNotEmpty()){
                $quantity = Auth::user()->cart->quantity;
            }
        }

        return view('aboutus', ['auth'=> $auth, 'role' => $role, 'user' => $user, 'quantity' => $quantity]);
    }
}
