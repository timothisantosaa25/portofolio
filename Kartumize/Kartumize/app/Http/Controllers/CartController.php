<?php

namespace App\Http\Controllers;

use App\Cart;
use App\Design;
use App\Item;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;

class CartController extends Controller
{
    public function viewCartPage(Request $request){
        $designs = Design::all();

        $search = $request->input('search');

        $auth = Auth::check();
        $role = 'guest';
        $user = '';
        $quantity = 0;
        $items = Item::where('cart_id','like', -1)->paginate();

        if($auth){
            $role = Auth::user()->role;
            $user = Auth::user()->username;
            $cart = Cart::where('user_id','like', Auth::user()->id)->paginate();
            if($cart->isNotEmpty()){
                $items = Item::where('cart_id','like', Auth::user()->cart->id)->paginate();
                $quantity = Auth::user()->cart->quantity;
            }
        }

        if($search != ''){
            $designs = Design::where('name', 'like', "%$search%")->paginate(4);
            return view('details', ['designs' => $designs, 'auth'=> $auth, 'role' => $role, 'user' => $user, 'quantity' => $quantity]);
        }
        else{
            return view('viewCart', ['designs' => $designs, 'items' => $items, 'auth'=> $auth, 'role' => $role, 'user' => $user, 'quantity' => $quantity]);
        }
    }

    public function addToCartPage(Request $request, $id){
        $designs = Design::where('id', 'like', $id)->paginate();

        $search = $request->input('search');

        $auth = Auth::check();
        $role = 'guest';
        $user = '';
        $quantity = 0;

        if($auth){
            $role = Auth::user()->role;
            $user = Auth::user()->username;
            $cart = Cart::where('user_id','like', Auth::user()->id)->paginate();
            if($cart->isNotEmpty()){
                $quantity = Auth::user()->cart->quantity;
            }
        }

        if($search != ''){
            $designs = Design::where('name', 'like', "%$search%")->paginate(3);
            return view('details', ['designs' => $designs, 'auth'=> $auth, 'role' => $role, 'user' => $user, 'quantity' => $quantity]);
        }
        else{
            return view('addToCart', ['designs' => $designs, 'auth'=> $auth, 'role' => $role, 'user' => $user, 'quantity' => $quantity]);
        }
    }

    public function addToCart(Request $request, $id){
        $request->validate([
            'quantity' => 'required|integer|min:1|',
        ]);

        $cart = Cart::where('user_id','like', Auth::user()->id)->paginate();

        if($cart->isEmpty()){
            Cart::create([
                'user_id' => Auth::user()->id, 
                'quantity'=> 0, 
            ]);
        }

        $items = Item::where('cart_id', 'like', Auth::user()->cart->id)
                    ->where('design_id', 'like', $id)->paginate();

        if($items->isEmpty()){
            Item::create([
                'cart_id' => Auth::user()->cart->id, 
                'design_id' => $id, 
                'quantity'=> $request->quantity,
            ]);

            Cart::where('user_id',Auth::user()->id)->update([
                'quantity' => Auth::user()->cart->quantity + 1,
            ]);
        }
        else{
            Item::where('cart_id', 'like', Auth::user()->cart->id)
                ->where('design_id', 'like', $id)->update([
                    'quantity' => $request->quantity
                ]);
        }
        return redirect('/');
    }

    public function editCartItem(Request $request){
        $request->validate([
            'quantity' => 'required|integer|min:1|',
        ]);

        Item::where('cart_id',$request->cid)
            ->where('design_id',$request->pid)->update([
                'quantity'=> $request->quantity,
            ]);
        
        return back();
    }

    public function deleteCartItem(Request $request){
        Item::where('cart_id', $request->cid)
            ->where('design_id', $request->pid)->delete();

        Cart::where('user_id', Auth::user()->id)->update([
            'quantity' => Auth::user()->cart->quantity - 1,
        ]);
        
        return redirect('/');
    }

    public function checkout(){
        
        Item::where('cart_id', Auth::user()->cart->id)->delete();
        Cart::where('id', Auth::user()->cart->id)->delete();

        return redirect('/');
    }
}
