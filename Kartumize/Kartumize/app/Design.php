<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Design extends Model
{
    public function item(){
        return $this->belongsToMany(Item::class);
    }

    public function category(){
        return $this->hasMany(Category::class, 'id', 'category_id');
    }
    
    protected $fillable = [
        'category_id', 'name', 'description', 'price', 'image', 
    ];

    protected $table = 'designs';
}
