<?php

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;

class CategorySeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('category')->insert([
            ['name' => 'Anime'],    //1
            ['name' => 'Brands'],   //2
            ['name' => 'Movies'],   //3
            ['name' => 'Games']     //4
        ]);
    }
}
