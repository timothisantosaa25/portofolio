<?php

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;

class DesignSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('designs')->insert([
            ['category_id' => 1, 'name' => 'My Hero Academia', 'description' => 'My Hero Academia Card Design', 'price' => 50000, 'image' => './assets/Anime1.png'],
            ['category_id' => 1, 'name' => 'Demon Slayer', 'description' => 'Demon Slayer Card Design', 'price' => 50000, 'image' => './assets/Anime2.png'],
            ['category_id' => 1, 'name' => 'One Piece', 'description' => 'One Piece Card Design', 'price' => 50000, 'image' => './assets/Anime3.png'],
            ['category_id' => 1, 'name' => 'Fate/Grand Order', 'description' => 'Fate/Grand Order Card Design', 'price' => 50000, 'image' => './assets/Anime4.png'],
            ['category_id' => 2, 'name' => 'Apple', 'description' => 'Apple Card Design', 'price' => 50000, 'image' => './assets/Brand1.png'],
            ['category_id' => 2, 'name' => 'Google', 'description' => 'Google Card Design', 'price' => 50000, 'image' => './assets/Brand2.png'],
            ['category_id' => 2, 'name' => 'Samsung', 'description' => 'Samsung Card Design', 'price' => 50000, 'image' => './assets/Brand3.png'],
            ['category_id' => 2, 'name' => 'Nike', 'description' => 'Nike Card Design', 'price' => 50000, 'image' => './assets/Brand4.png'],
            ['category_id' => 3, 'name' => 'Batman', 'description' => 'Batman Card Design', 'price' => 50000, 'image' => './assets/Film1.png'],
            ['category_id' => 3, 'name' => 'Iron Man', 'description' => 'Iron Man Card Design', 'price' => 50000, 'image' => './assets/Film2.png'],
            ['category_id' => 3, 'name' => 'Deadpool', 'description' => 'Deadpool Card Design', 'price' => 50000, 'image' => './assets/Film3.png'],
            ['category_id' => 3, 'name' => 'Miles Morales', 'description' => 'Miles Morales Card Design', 'price' => 50000, 'image' => './assets/Film4.png'],
            ['category_id' => 4, 'name' => 'Genshin Impact', 'description' => 'Genshin Impact Card Design', 'price' => 50000, 'image' => './assets/Game1.png'],
            ['category_id' => 4, 'name' => 'Arknights', 'description' => 'Arknights Card Design', 'price' => 50000, 'image' => './assets/Game2.png'],
            ['category_id' => 4, 'name' => 'Call Of Duty', 'description' => 'Call Of Duty Card Design', 'price' => 50000, 'image' => './assets/Game3.png'],
            ['category_id' => 4, 'name' => 'Among Us', 'description' => 'Among Us Card Design', 'price' => 50000, 'image' => './assets/Game4.png'],

        ]);
    }
}
