<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Kartumize</title>    
    <link rel="stylesheet" href="https://cdn.jsdelivr.net/npm/bootstrap@4.5.3/dist/css/bootstrap.min.css" integrity="sha384-TX8t27EcRE3e/ihU7zmQxVncDAy5uIKz4rEkgIXeMed4M0jlfIDPvg6uqKI2xXr2" crossorigin="anonymous"></head>
    <script src="https://code.jquery.com/jquery-3.5.1.slim.min.js" integrity="sha384-DfXdz2htPH0lsSSs5nCTpuj/zy4C+OGpamoFVy38MVBnE+IbbVYUew+OrCXaRkfj" crossorigin="anonymous"></script>
    <script src="https://cdn.jsdelivr.net/npm/bootstrap@4.5.3/dist/js/bootstrap.bundle.min.js" integrity="sha384-ho+j7jyWK8fNQe+A12Hb8AhRq26LrZ/JpcUGGOn+Y7RsweNrtN/tE3MoK7ZeZDyx" crossorigin="anonymous"></script>
<body class="bg-dark">
    <nav class="navbar navbar-expand-lg navbar-dark bg-dark">
        <a class="navbar-brand text-danger" href="{{ URL::to('/adminHome/')}}">Kartumize</a>
        <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarSupportedContent" aria-controls="navbarSupportedContent" aria-expanded="false" aria-label="Toggle navigation">
            <span class="navbar-toggler-icon"></span>
        </button>

        <div class="collapse navbar-collapse" id="navbarSupportedContent">
            <ul class="navbar-nav mr-auto">
                <li class="nav-item dropdown">
                    <a class="nav-link dropdown-toggle" href="" id="navbarDropdown" role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                        Design
                    </a>
                    <div class="dropdown-menu" aria-labelledby="navbarDropdown">
                        <a class="dropdown-item" href="{{ URL::to('/addDesign/')}}">Add Design</a>
                        <a class="dropdown-item" href="{{ URL::to('/viewDesign/')}}">Show All Designs</a>
                    </div>
                </li>
                <li class="nav-item dropdown">
                    <a class="nav-link dropdown-toggle" href="" id="navbarDropdown" role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                        Category
                    </a>
                    <div class="dropdown-menu" aria-labelledby="navbarDropdown">
                        <a class="dropdown-item" href="{{ URL::to('/addCategory/')}}">Add Category</a>
                        <a class="dropdown-item" href="{{ URL::to('/viewCategory/')}}">Show All Category</a>
                    </div>
                </li>
            </ul>
            <ul class="navbar-nav">
                <li class="nav-item dropdown">
                    <a class="nav-link dropdown-toggle text-danger" href="" id="navbarDropdown" role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                        Admin
                    </a>
                    <div class="dropdown-menu" aria-labelledby="navbarDropdown">
                        <a class="dropdown-item" href="{{ URL::to('/logout/')}}">Logout</a>
                    </div>
                </li>
            </ul>
        </div>
    </nav>
    <br>
    <h1 class="text-center text-light">Category</h1>
    <br>
    <table class="table table-borderless table-dark">
        @foreach($category as $c)
            <tbody class="text-center ">
                <td>
                    <a href="{{ URL::to('/viewCategoryDetail/'.$c->id) }}" class="btn text-light" role="button" aria-pressed="true">{{$c->name}}</a>
                </td>
            </tbody>
        @endforeach
    </table>
    <h1 class="text-center text-light">Product</h1>
    <br>
    <table class="table table-striped text-light">
        <thead class="thead-dark">
            <tr class="text-center">
                <th style="width:105px">Design ID</th>
                <th style="width:260px">Design Image</th>
                <th style="width:140px">Design Name</th>
                <th style="width:130px">Design Price</th>
                <th style="width:400px">Design Description</th>
            </tr>
        </thead>
        <tbody>
            @foreach($designs as $d)
                <tr class="text-center">
                    <td style="vertical-align: middle;">{{$d->id}}</td>
                    <td style="vertical-align: middle;"><img src="\{{$d->image}}" class="rounded" alt="" height="auto" width="200"></td>
                    <td style="vertical-align: middle;">{{$d->name}}</td>
                    <td style="vertical-align: middle;">{{$d->price}}</td>
                    <td style="vertical-align: middle;">{{$d->description}}</td>
                </tr>
            @endforeach
        </tbody>
    </table>
</body>
</html>