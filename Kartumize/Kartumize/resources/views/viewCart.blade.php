<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Kartumize</title>    
    <link rel="stylesheet" href="https://cdn.jsdelivr.net/npm/bootstrap@4.5.3/dist/css/bootstrap.min.css" integrity="sha384-TX8t27EcRE3e/ihU7zmQxVncDAy5uIKz4rEkgIXeMed4M0jlfIDPvg6uqKI2xXr2" crossorigin="anonymous"></head>
    <script src="https://code.jquery.com/jquery-3.5.1.slim.min.js" integrity="sha384-DfXdz2htPH0lsSSs5nCTpuj/zy4C+OGpamoFVy38MVBnE+IbbVYUew+OrCXaRkfj" crossorigin="anonymous"></script>
    <script src="https://cdn.jsdelivr.net/npm/bootstrap@4.5.3/dist/js/bootstrap.bundle.min.js" integrity="sha384-ho+j7jyWK8fNQe+A12Hb8AhRq26LrZ/JpcUGGOn+Y7RsweNrtN/tE3MoK7ZeZDyx" crossorigin="anonymous"></script>
<body class="bg-dark">
    <nav class="navbar navbar-expand-lg navbar-dark bg-dark">
        <a class="navbar-brand mb-0 h1 text-danger" href="{{ URL::to('/home/')}}">Kartumize</a>
        <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarSupportedContent" aria-controls="navbarSupportedContent" aria-expanded="false" aria-label="Toggle navigation">
            <span class="navbar-toggler-icon"></span>
        </button>

        <div class="collapse navbar-collapse" id="navbarSupportedContent">
            <ul class="navbar-nav mr-auto">
                <li class="nav-item active">
                    <a class="nav-link" href="{{ URL::to('/')}}">Home</a>
                </li>
                <li class="nav-item active">
                    <a class="nav-link" href="{{ URL::to('/designs/')}}">Designs</a>
                </li>
                <li class="nav-item active">
                    <a class="nav-link" href="{{ URL::to('/aboutus/')}}">About Us</a>
                </li>
            </ul>
            <ul class="navbar-nav">
                @if($auth)
                    <li class="nav-item active">
                        <a class="nav-link" href="{{ URL::to('/viewCart/')}}">Cart: {{$quantity}}</a>
                    </li>

                    <li class="nav-item dropdown">
                        <a class="nav-link dropdown-toggle" href="" id="navbarDropdown" role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                            {{$user}}
                        </a>
                        <div class="dropdown-menu" aria-labelledby="navbarDropdown">
                            <a class="dropdown-item" href="{{ URL::to('/logout/')}}">Logout</a>
                        </div>
                    </li>
                @else
                    <li class="nav-item active">
                        <a class="nav-link" href="{{ URL::to('/login/')}}">Login</a>
                    </li>
                @endif
            </ul>
        </div>
    </nav>
    <br>
    <table class="table table-dark table-borderless">
        <tbody>
            @foreach($items as $i)
                @foreach($designs as $d)
                    @if($i->design_id == $d->id)
                        <td style="text-align:center;"><img src="{{$d->image}}" class="rounded" alt="" height="auto" width="320"></td>
                    @endif
                @endforeach
            @endforeach
        </tbody>
        <tbody>
            @foreach($items as $i)
                @foreach($designs as $d)
                    @if($i->p_id == $d->id)
                        <td style="text-align:center; font-size: 20px;">{{$d->name}}</td>
                    @endif
                @endforeach
            @endforeach
        </tbody>
        <tbody>
            @foreach($items as $i)
                @foreach($designs as $d)
                    @if($i->design_id == $d->id)
                        <td style="text-align:center; font-size: 20px;">Price: IDR.{{$d->price * $i->quantity}}</td>
                    @endif
                @endforeach
            @endforeach
        </tbody>
        <tbody>
            @foreach($items as $i)
                @foreach($designs as $d)
                    @if($i->design_id == $d->id)
                        <td style="text-align:center; font-size: 20px;">Quantity: {{$i->quantity}}</td>
                    @endif
                @endforeach
            @endforeach
        </tbody>
        <tbody>
            @foreach($items as $i)
                @foreach($designs as $d)
                    @if($i->design_id == $d->id)
                        <td style="text-align:center; font-size: 20px;">
                            <form method="POST" action="{{ URL::to('/deleteItem/')}}">
                                {{ csrf_field() }}
                                <input type="hidden" name="cid" value="{{$i->cart_id}}">
                                <input type="hidden" name="pid" value="{{$i->design_id}}">
                                <input type="submit" class="btn btn-danger" value="Delete">
                            </form>
                            <br>
                            <form method="POST" action="{{ URL::to('/editItem/')}}">
                                {{ csrf_field() }}
                                <input type="hidden" name="cid" value="{{$i->cart_id}}">
                                <input type="hidden" name="pid" value="{{$i->design_id}}">
                                <label for="quantity" class="col-form-label">Quantity:</label>
                                <input type="number" class="row-sm-1" style="width:40px" name="quantity" id="quantity">
                                @error('quantity')
                                    <div class="alert-danger">{{ $message }}</div>
                                @enderror
                                <input type="submit" class="btn btn-danger" value="Edit">
                            </form>
                        </td>
                    @endif
                @endforeach
            @endforeach
        </tbody>
    </table>
    @if($items->isNotEmpty())
    <div style="display: table;margin: 0 auto;">
    <div style="width:100%;">
        <form method="POST" action="{{ URL::to('/checkout/')}}">
            {{ csrf_field() }}
            <input type="submit" class="btn btn-danger" value="Checkout">
        </form>
    </div>
    </div>
    @endif
</body>
</html>