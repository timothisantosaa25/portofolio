<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Kartumize</title>    
    <link rel="stylesheet" href="https://cdn.jsdelivr.net/npm/bootstrap@4.5.3/dist/css/bootstrap.min.css" integrity="sha384-TX8t27EcRE3e/ihU7zmQxVncDAy5uIKz4rEkgIXeMed4M0jlfIDPvg6uqKI2xXr2" crossorigin="anonymous"></head>
    <script src="https://code.jquery.com/jquery-3.5.1.slim.min.js" integrity="sha384-DfXdz2htPH0lsSSs5nCTpuj/zy4C+OGpamoFVy38MVBnE+IbbVYUew+OrCXaRkfj" crossorigin="anonymous"></script>
    <script src="https://cdn.jsdelivr.net/npm/bootstrap@4.5.3/dist/js/bootstrap.bundle.min.js" integrity="sha384-ho+j7jyWK8fNQe+A12Hb8AhRq26LrZ/JpcUGGOn+Y7RsweNrtN/tE3MoK7ZeZDyx" crossorigin="anonymous"></script>
<body class="bg-dark">
    <nav class="navbar navbar-expand-lg navbar-dark bg-dark justify-content-between">
        <a class="navbar-brand mb-0 h1 text-danger" href="{{ URL::to('/home/')}}">Kartumize</a>
        <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarSupportedContent" aria-controls="navbarSupportedContent" aria-expanded="false" aria-label="Toggle navigation">
            <span class="navbar-toggler-icon"></span>
        </button>

        <div class="collapse navbar-collapse" id="navbarSupportedContent">
            <ul class="navbar-nav mr-auto">
                <li class="nav-item active">
                    <a class="nav-link" href="{{ URL::to('/')}}">Home</a>
                </li>
                <li class="nav-item active">
                    <a class="nav-link" href="{{ URL::to('/designs/')}}">Designs</a>
                </li>
                <li class="nav-item active">
                    <a class="nav-link" href="{{ URL::to('/aboutus/')}}">About Us</a>
                </li>
              
            </ul>
            <ul class="navbar-nav">
                @if($auth)
                <li class="nav-item active">
                    <a class="nav-link" href="{{ URL::to('/submit/')}}">Submit Your Design</a>
                </li>
                    <li class="nav-item active">
                        <a class="nav-link" href="{{ URL::to('/viewCart/')}}">Cart: {{$quantity}}</a>
                    </li>
                   
                    <li class="nav-item dropdown">
                        <a class="nav-link dropdown-toggle" href="" id="navbarDropdown" role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                            {{$user}}
                        </a>
                        <div class="dropdown-menu" aria-labelledby="navbarDropdown">
                            <a class="dropdown-item" href="{{ URL::to('/logout/')}}">Logout</a>
                        </div>
                    </li>
                @else
                    <li class="nav-item active">
                        <a class="nav-link" href="{{ URL::to('/login/')}}">Login</a>
                    </li>
                    <li class="nav-item active">
                        <a class="nav-link" href="{{ URL::to('/register/')}}">Register</a>
                    </li>
                @endif
            </ul>
        </div>
</nav>
    <div class="p-3 mb-2 bg-dark text-light">
        <div class="p-3 bg-danger">
            <div class="p-3 bg-dark">
                <h3 class="p-1">
                    Anime
                </h3>
                <div class="row">
                @foreach ($anime as $anm)
                    <div class="col-md-3">
                    <div class="container">
                    <img src="{{$anm->image}}" alt="{{$anm->image}}" class="rounded" style="height: 100%; width:100%">
                    <div class="p-2 container text-center">{{$anm->name}}</div>
                    @if($auth)
                    <a href="{{ URL::to('/details/'.$anm->id) }}"  type="button" class="btn btn-danger" style="width: 100%">CHOOSE</a>
                    @else
                    <a href="{{ URL::to('/login/')}}"  type="button" class="btn btn-danger" style="width: 100%">CHOOSE</a>
                    @endif
                </div>
                </div>
                @endforeach
            </div>
            <br>
                <h3 class="p-1">
                    Brands
                </h3>
                <div class="row">
                @foreach ($brand as $brd)
                    <div class="col-md-3">
                        <div class="container">
                    <img src="{{$brd->image}}" alt="{{$brd->image}}" class="rounded" style="height: 100%; width:100%">
                    <div class="p-2 container text-center">{{$brd->name}}</div>
                    @if($auth)
                    <a href="{{ URL::to('/details/'.$brd->id) }}"  type="button" class="btn btn-danger" style="width: 100%">CHOOSE</a>
                    @else
                    <a href="{{ URL::to('/login/')}}"  type="button" class="btn btn-danger" style="width: 100%">CHOOSE</a>
                    @endif
                </div>
                </div>
                @endforeach
            </div>
            <br>
                <h3 class="p-1"> 
                    Movies
                </h3>
                <div class="row">
                @foreach ($movie as $mov)
                    <div class="col-md-3">
                    <div class="container">
                    <img src="{{$mov->image}}" alt="{{$mov->image}}" class="rounded" style="height: 100%; width:100%">
                    <div class="p-2 container text-center">{{$mov->name}}</div>
                    @if($auth)
                    <a href="{{ URL::to('/details/'.$mov->id) }}"  type="button" class="btn btn-danger" style="width: 100%">CHOOSE</a>
                    @else
                    <a href="{{ URL::to('/login/')}}"  type="button" class="btn btn-danger" style="width: 100%">CHOOSE</a>
                    @endif
                </div>
            </div>
                @endforeach
            </div>
            <br>
            <h3 class="p-1"> 
                    Games
                </h3>
                <div class="row">
                @foreach ($game as $gme)
                    <div class="col-md-3">
                    <div class="container">
                    <img src="{{$gme->image}}" alt="{{$gme->image}}" class="rounded" style="height: 100%; width:100%">
                    <div class="p-2 container text-center">{{$gme->name}}</div>
                    @if($auth)
                    <a href="{{ URL::to('/details/'.$gme->id) }}"  type="button" class="btn btn-danger" style="width: 100%">CHOOSE</a>
                    @else
                    <a href="{{ URL::to('/login/')}}"  type="button" class="btn btn-danger" style="width: 100%">CHOOSE</a>
                    @endif
                </div>
            </div>
                @endforeach
            </div>
            </div>
        </div>
    </div>

</body>
</html>