<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Kartumize</title>
    <link rel="stylesheet" href="https://cdn.jsdelivr.net/npm/bootstrap@4.5.3/dist/css/bootstrap.min.css" integrity="sha384-TX8t27EcRE3e/ihU7zmQxVncDAy5uIKz4rEkgIXeMed4M0jlfIDPvg6uqKI2xXr2" crossorigin="anonymous">
    <script src="https://code.jquery.com/jquery-3.5.1.slim.min.js" integrity="sha384-DfXdz2htPH0lsSSs5nCTpuj/zy4C+OGpamoFVy38MVBnE+IbbVYUew+OrCXaRkfj" crossorigin="anonymous"></script>
    <script src="https://cdn.jsdelivr.net/npm/bootstrap@4.5.3/dist/js/bootstrap.bundle.min.js" integrity="sha384-ho+j7jyWK8fNQe+A12Hb8AhRq26LrZ/JpcUGGOn+Y7RsweNrtN/tE3MoK7ZeZDyx" crossorigin="anonymous"></script>
</head>
<body class="bg-dark">
    <nav class="navbar navbar-expand-lg navbar-dark bg-dark">
        <a class="navbar-brand mb-0 h1 text-danger" href="{{ URL::to('/home/')}}">Kartumize</a>
        <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarSupportedContent" aria-controls="navbarSupportedContent" aria-expanded="false" aria-label="Toggle navigation">
            <span class="navbar-toggler-icon"></span>
        </button>

        <div class="collapse navbar-collapse" id="navbarSupportedContent">
            <ul class="navbar-nav mr-auto">
                <li class="nav-item active">
                    <a class="nav-link" href="{{ URL::to('/')}}">Home</a>
                </li>
                <li class="nav-item active">
                    <a class="nav-link" href="{{ URL::to('/designs/')}}">Designs</a>
                </li>
        
            </ul>
            <ul class="navbar-nav">
                <li class="nav-item active">
                    <a class="nav-link" href="{{ URL::to('/login/')}}">Login</a>
                </li>
                <li class="nav-item active">
                    <a class="nav-link" href="{{ URL::to('/register/')}}">Register</a>
                </li>
            </ul>
        </div>
    </nav>
    <div style="display: table;margin: 0 auto;">
    <div style="width:100%;">
    <table class="table table-dark table-borderless">
        <thead class="thead-dark">
            <tr>
                <th >Register</th>
            </tr>
        </thead>
        <tbody>
            <td>
                <form method="POST" action="">
                    {{ csrf_field() }}
                    <div class="form-group row">
                        <label for="username" class="col-sm-3 col-form-label">Name</label>
                        <div class="col-sm-10">
                            <input type="text" style="width:500px;" class="form-control" placeholder="Name" name="username" id="username">
                            @error('username')
                                <div class="alert-danger">{{ $message }}</div>
                            @enderror
                        </div>
                    </div>
                    <div class="form-group row">
                        <label for="email" class="col-sm-3 col-form-label">E-Mail Address</label>
                        <div class="col-sm-10">
                            <input type="text" style="width:500px;" class="form-control" placeholder="Email" name="email" id="email">
                            @error('email')
                                <div class="alert-danger">{{ $message }}</div>
                            @enderror
                        </div>
                    </div>
                    <div class="form-group row">
                        <label for="password" class="col-sm-3 col-form-label">Password</label>
                        <div class="col-sm-10">
                            <input type="password" style="width:500px;" class="form-control" placeholder="Password" name="password" id="password">
                            @error('password')
                                <div class="alert-danger">{{ $message }}</div>
                            @enderror
                        </div>
                    </div>
                    <div class="form-group row">
                        <label for="confirmPassword" class="col-sm-3 col-form-label">Confirm Password</label>
                        <div class="col-sm-10">
                            <input type="password" style="width:500px;" class="form-control" placeholder="Confirm Password" name="confirmPassword" id="confirmPassword">
                            @error('confirmPassword')
                                <div class="alert-danger">{{ $message }}</div>
                            @enderror
                        </div>
                    </div>
                    <input type="submit" class="btn btn-danger" value="Register">
                </form>
            </td>
        </tbody>
    </table>
    </div>
    </div>
</body>
</html>