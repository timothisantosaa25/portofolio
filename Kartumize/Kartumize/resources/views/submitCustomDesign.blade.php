<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Kartumize</title>    
    <link rel="stylesheet" href="https://cdn.jsdelivr.net/npm/bootstrap@4.5.3/dist/css/bootstrap.min.css" integrity="sha384-TX8t27EcRE3e/ihU7zmQxVncDAy5uIKz4rEkgIXeMed4M0jlfIDPvg6uqKI2xXr2" crossorigin="anonymous"></head>
    <script src="https://code.jquery.com/jquery-3.5.1.slim.min.js" integrity="sha384-DfXdz2htPH0lsSSs5nCTpuj/zy4C+OGpamoFVy38MVBnE+IbbVYUew+OrCXaRkfj" crossorigin="anonymous"></script>
    <script src="https://cdn.jsdelivr.net/npm/bootstrap@4.5.3/dist/js/bootstrap.bundle.min.js" integrity="sha384-ho+j7jyWK8fNQe+A12Hb8AhRq26LrZ/JpcUGGOn+Y7RsweNrtN/tE3MoK7ZeZDyx" crossorigin="anonymous"></script>
</head>
<body class="bg-dark">
<nav class="navbar navbar-expand-lg navbar-dark bg-dark">
        <a class="navbar-brand mb-0 h1 text-danger" href="{{ URL::to('/home/')}}">Kartumize</a>
        <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarSupportedContent" aria-controls="navbarSupportedContent" aria-expanded="false" aria-label="Toggle navigation">
            <span class="navbar-toggler-icon"></span>
        </button>

        <div class="collapse navbar-collapse" id="navbarSupportedContent">
            <ul class="navbar-nav mr-auto">
                <li class="nav-item active">
                    <a class="nav-link" href="{{ URL::to('/')}}">Home</a>
                </li>
                <li class="nav-item active">
                    <a class="nav-link" href="{{ URL::to('/designs/')}}">Designs</a>
                </li>
                <li class="nav-item active">
                    <a class="nav-link" href="{{ URL::to('/aboutus/')}}">About Us</a>
                </li>
            </ul>
            <ul class="navbar-nav">
                @if($auth)
                <li class="nav-item active">
                    <a class="nav-link" href="{{ URL::to('/submit/')}}">Submit</a>
                </li>
                    <li class="nav-item active">
                        <a class="nav-link" href="{{ URL::to('/viewCart/')}}">Cart: {{$quantity}}</a>
                    </li>
                    <li class="nav-item dropdown">
                        <a class="nav-link dropdown-toggle" href="" id="navbarDropdown" role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                            {{$user}}
                        </a>
                        <div class="dropdown-menu" aria-labelledby="navbarDropdown">
                            <a class="dropdown-item" href="{{ URL::to('/logout/')}}">Logout</a>
                        </div>
                    </li>
                @else
                    <li class="nav-item active">
                        <a class="nav-link" href="{{ URL::to('/login/')}}">Login</a>
                    </li>
                    <li class="nav-item active">
                        <a class="nav-link" href="{{ URL::to('/register/')}}">Register</a>
                    </li>
                @endif
            </ul>
        </div>
    </nav>
    <br>
    <h1 class="text-center text-light">Submit Design</h1>
    <table class="table table-borderless">
        <tbody>
            <td>
                <form method="POST" action="" enctype="multipart/form-data">
                    {{ csrf_field() }}
                    <div style="display: table;margin: 0 auto;">
                    <div style="width:100%;">
                    <div class="form-group text-center">
                        <label for="name" class="text-light ">Design Name</label>
                        <input type="text" style="width:500px;" class="form-control" placeholder="Design Name" name="name" id="name">
                        @error('name')
                            <div class="alert-danger">{{ $message }}</div>
                        @enderror
                    </div>
                    </div>
                    </div>
                    <div class="form-group text-center">
                        <label for="image" class="text-light">Choose File</label>
                        <div style="margin-left: 43%;">
                            <input type="file" class="form-control-file text-light" name="image" id="image">
                        </div>
                        @error('image')
                            <div class="alert-danger">{{ $message }}</div>
                        @enderror
                    </div>
                    <div class="form-group text-center">
                        <input type="submit" class="btn btn-outline-danger" value="Submit Design">
                    </div>
                </form>
            </td>
        </tbody>
    </table>
</body>
</html>